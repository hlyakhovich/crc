package org.example.crc16;

import org.junit.jupiter.api.Test;

import static org.example.crc16.CRC16Calculator.calculate;
import static org.junit.jupiter.api.Assertions.*;

class CRC16CalculatorTest {

    @Test
    public void testCalc() {
        assertEquals("677F", calculate("011000110003061AC4BAD0"));
    }

}