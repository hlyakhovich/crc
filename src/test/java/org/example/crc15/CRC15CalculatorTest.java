package org.example.crc15;

import org.junit.jupiter.api.Test;

import static org.example.crc15.CRC15Calculator.calculate;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CRC15CalculatorTest {

    @Test
    public void testCalc() {
        assertEquals("059E", calculate("001100010011001000110011001101000011010100110110001101110011100000111001"));
    }

}