package org.example.crc15;

import at.favre.lib.bytes.Bytes;

public class CRC15Calculator {

    private static final Bytes HEX_4599 = Bytes.parseHex("4599");

    public static String calculate(String input) {
        final Bytes bytes = Bytes.parseBinary(input);

        Bytes crcRg = Bytes.allocate(2);
        for (int i = bytes.lengthBit() - 1; i >= 0; i--) {
            final boolean crcNxt = bytes.bitAt(i) ^ crcRg.bitAt(14);
            crcRg = crcRg.leftShift(1);
            crcRg = crcRg.switchBit(0, false);
            if (crcNxt) {
                crcRg = crcRg.xor(HEX_4599);
            }
        }
        crcRg = crcRg.switchBit(15, false);

        return crcRg.encodeHex(true);
    }

}
