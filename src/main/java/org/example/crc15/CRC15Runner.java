package org.example.crc15;

import at.favre.lib.bytes.Bytes;
import org.example.crc15.model.CommandLineArgs;
import org.example.crc15.model.Result;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

/**
 * Helper class to calculate CRC-15 used in CAN.
 * Expected CRC for input string "123456789" is expected to be 0x59E, as per
 * <a href="https://infineon.github.io/mtb-pdl-cat2/pdl_api_reference_manual/html/group__group__crypto__lld__crc__functions.html">CAT2 Peripheral Driver Library: Cyclic redundancy code (CRC)</a>
 */
public class CRC15Runner {

    private static final int MAX_BINARY_LENGTH_DIGITS = 96;

    public static void main(String[] args) {
        final CommandLineArgs cmdArgs = getArgs();
        validateArguments(cmdArgs);
        final var result = run(cmdArgs.input(), cmdArgs.numRuns());
        printResults(result);
    }

    private static CommandLineArgs getArgs() {
        System.out.println("Usage: binaryInput numberOfRuns");
        final var scanner = new Scanner(System.in);
        final String[] argArr = scanner.nextLine().split(" ");
        scanner.close();
        return new CommandLineArgs(argArr[0], Integer.parseInt(argArr[1]));
    }

    private static void validateArguments(CommandLineArgs cmdArgs) {
        validateNumRuns(cmdArgs.numRuns());
        validateInput(cmdArgs.input());
    }

    private static void validateNumRuns(int numRuns) {
        if (numRuns < 1 || numRuns > 1_000_000_000) {
            throw new IllegalArgumentException("Illegal number of runs: " + numRuns);
        }
    }

    private static void validateInput(String input) {
        if (input.length() > MAX_BINARY_LENGTH_DIGITS) {
            throw new IllegalArgumentException("Illegal input length: " + input.length());
        }
        Bytes.parseBinary(input);
    }

    private static Result run(String input, int times) {
        final Instant start = Instant.now();
        for (int i = 0; i < times; i++) {
            CRC15Calculator.calculate(input);
        }
        final Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        return new Result(CRC15Calculator.calculate(input), timeElapsed);
    }

    private static void printResults(Result result) {
        System.out.println("CRC: " + result.crc());
        System.out.printf("Time taken: %s ms", result.timeTakenMs());
    }

}
