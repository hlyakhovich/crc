package org.example.crc15.model;

public record CommandLineArgs(String input, int numRuns) {
}
