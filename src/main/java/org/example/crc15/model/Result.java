package org.example.crc15.model;

public record Result(String crc, long timeTakenMs) {
}
