package org.example.breaker.model;

public record CommandLineArgs(String targetCrc, int keySizeBits, int requiredNumCollisions) {
}
