package org.example.breaker;

import org.example.breaker.model.CommandLineArgs;
import org.example.crc16.CRC16Calculator;

import java.math.BigInteger;
import java.util.*;

/**
 * Brute-forces random input strings until a number of collisions matching the input CRC-16 is found.
 */
public class CRCBreaker {

    public static void main(String[] args) {
        final CommandLineArgs cmdArgs = getArgs();
        final var collisions = findCollisions(cmdArgs.targetCrc(), cmdArgs.keySizeBits(), cmdArgs.requiredNumCollisions());
        printResults(cmdArgs.targetCrc(), collisions);
    }

    private static CommandLineArgs getArgs() {
        System.out.println("Usage: targetCRC keySizeBits collisionNumber");
        final var scanner = new Scanner(System.in);
        final String[] argArr = scanner.nextLine().split(" ");
        scanner.close();
        return new CommandLineArgs(argArr[0], Integer.parseInt(argArr[1]), Integer.parseInt(argArr[2]));
    }

    public static Set<String> findCollisions(String targetCRC, int keySizeBits, int requiredNumCollisions) {
        final Set<String> resultList = new HashSet<>();
        int collisionsFound = 0;

        while (true) {
            final var candidate = new BigInteger(keySizeBits, new Random());
            final var formattedCandidate = String.format("%0" + keySizeBits / 4 + "x", candidate).toUpperCase();
            final var crc = CRC16Calculator.calculate(formattedCandidate);
            if (crc.equals(targetCRC)) {
                collisionsFound++;
                resultList.add(formattedCandidate);
                if (requiredNumCollisions == collisionsFound) {
                    break;
                }
            }
        }
        return resultList;
    }

    private static void printResults(String targetCRC, Set<String> collisions) {
        System.out.println("Keys that have a CRC-16 equal to " + targetCRC + ":");
        for (String key : collisions) {
            System.out.println(key);
        }
    }

}
